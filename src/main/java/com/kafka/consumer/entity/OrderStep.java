package com.kafka.consumer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kafka.consumer.enums.Process;
import com.kafka.consumer.enums.StepStatus;

import javax.persistence.*;

@Entity
public class OrderStep {
    @Id
    private Long id;
    private Process process;
    @Enumerated(EnumType.ORDINAL)
    private StepStatus status;
    @ManyToOne
    @JoinColumn(name = "order_id")
    @JsonIgnore
    private Order order;

    public OrderStep(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public StepStatus getStatus() {
        return status;
    }

    public void setStatus(StepStatus status) {
        this.status = status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
