package com.kafka.consumer.entity;

import com.kafka.consumer.enums.OrderState;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue
    private Long id;
    private String productName;
    private Integer amount;
    @Enumerated(EnumType.ORDINAL)
    private OrderState state; // pending // processing // fail // success
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<OrderStep> orderSteps = new ArrayList<>();

    public Order(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public List<OrderStep> getOrderSteps() {
        return orderSteps;
    }

    public void setOrderSteps(List<OrderStep> orderSteps) {
        this.orderSteps = orderSteps;
    }

    @PrePersist
    public void init(){
        this.state = OrderState.PENDING;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", amount=" + amount +
                ", state=" + state +
                ", orderSteps=" + orderSteps +
                '}';
    }
}
