package com.kafka.consumer.enums;

public enum StepStatus {
    PENDING,
    PROCESSING,
    FAIL,
    SUCCESS;
}
