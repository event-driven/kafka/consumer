package com.kafka.consumer.enums;

public enum OrderState {
    PENDING,
    PROCESSING,
    FAILED,
    SUCCESS;
}
