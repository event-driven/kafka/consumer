package com.kafka.consumer.repository;

import com.kafka.consumer.entity.OrderStep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStepRepository extends JpaRepository<OrderStep, Long> {
}
