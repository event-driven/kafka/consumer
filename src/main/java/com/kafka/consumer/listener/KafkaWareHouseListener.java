package com.kafka.consumer.listener;

import com.kafka.consumer.entity.Order;
import com.kafka.consumer.entity.OrderStep;
import static com.kafka.consumer.enums.Process.WAREHOUSE;
import com.kafka.consumer.enums.StepStatus;
import com.kafka.consumer.repository.OrderRepository;
import com.kafka.consumer.repository.OrderStepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class KafkaWareHouseListener {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderStepRepository orderStepRepository;
    @Autowired
    private KafkaTemplate<String, String> template;
    @Value(value = "${spring.kafka.topic.result}")
    private String topic;

    @Transactional
    @KafkaListener(topics = "process_businesses", containerFactory = "kafkaListenerContainerWareHouseFactory")
    public void listener(String message) throws InterruptedException {
        Order order = orderRepository.getById(Long.parseLong(message));
        OrderStep wareHouse = order.getOrderSteps().stream().filter(o -> o.getProcess() == WAREHOUSE).findFirst().orElse(null);
        wareHouse.setStatus(StepStatus.PROCESSING);
        orderStepRepository.saveAndFlush(wareHouse);
        Thread.sleep(10000l);
        wareHouse.setStatus(StepStatus.SUCCESS);
        orderStepRepository.save(wareHouse);
        template.send(topic, String.valueOf(order.getId()), String.valueOf(order.getId()));
    }
}
